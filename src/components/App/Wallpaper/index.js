import Vue from "vue";
import AppWrapper from '../AppWrapper/index.vue'
import store from '@/store'

import {mapState, mapMutations} from 'vuex'

const template = `<AppWrapper :id="this.id" :icon="this.icon" :name="this.name">
    <div id="jay-app-wallpaper">
      <div class="preview">
        <img :src="backgroundImg" alt="bg-preview">
      </div>
      <div class="candidates">
        <div
          v-for="(item,index) in backgroundImgList"
          :key="index"
          class="candidates-item"
          :class="{'candidates-item-selected':backgroundImg === item}"
          @click="onSelect($event,item,index)"
        >
          <img :src="item" :alt="'bg'+index">
        </div>
      </div>
    </div>
  </AppWrapper>`


let Wallpaper = Vue.extend({
  template,
  name: "app-wallpaper",
  components:{
    AppWrapper
  },
  data(){
    return{
      id:'app-wallpaper',
      icon:'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/ad5de446-06c4-4d3e-9502-a0fa240597c7.png',
      name:'壁纸'
    }
  },
  computed:{
    ...mapState('system',{
      backgroundImg: state => state.backgroundImg,
      backgroundImgList: state => state.backgroundImgList,
    })
  },
  methods:{
    onSelect(e,item,index){
      this.$store.commit('system/CHANGE_BACKGROUND_IMG',item)
    }
  }
})

const wallpaperInstance = new Wallpaper({store}).$mount()

export default wallpaperInstance