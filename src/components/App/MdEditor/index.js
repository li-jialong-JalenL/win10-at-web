import Vue from "vue";
import store from '@/store'

import {Editor} from '@bytemd/vue'
import AppWrapper from '../AppWrapper/index.vue'
import lang_cn from 'bytemd/lib/locales/zh_Hans.json'
import gfm from '@bytemd/plugin-gfm';
import breaks from '@bytemd/plugin-breaks';
import footnotes from '@bytemd/plugin-footnotes';
import frontmatter from '@bytemd/plugin-frontmatter';
import gemoji from '@bytemd/plugin-gemoji';
import highlight from '@bytemd/plugin-highlight';
import mediumZoom from '@bytemd/plugin-medium-zoom';
import '@/style/md-theme/github-markdown-css.css'
import 'highlight.js/scss/arta.scss'

const plugins = [
  gfm(),
  breaks(),
  footnotes(),
  frontmatter(),
  gemoji(),
  highlight(),
  mediumZoom()
  // Add more plugins here
];

const template = `
    <app-wrapper :id="id" :name="name" :icon="icon" :title="title">
      <div id="app-md-editor">
        <div class="app-md-editor-header">
          <div class="app-md-editor-header-left">
            <span class="app-md-editor-header-item" @click="onNewFile">新建</span>
            <span class="app-md-editor-header-item" @click="onOpenFile">打开</span>
            <span class="app-md-editor-header-item" @click="onSave">保存</span>
            <span class="app-md-editor-header-item" @click="onSaveAs">另存为</span>
          </div>
          <div class="app-md-editor-header-right">

          </div>
        </div>
        <Editor :value="value" :plugins="plugins" @change="onChange"  :locale="lang" :style="{width:'100%',height:'100%'}"/>
      </div>
    </app-wrapper>
`


let MdEditor = Vue.extend({
  template,
  name: "MdEditor",
  components:{
    AppWrapper,
    Editor
  },
  data(){
    return {
      id:"app-mdEditor",
      icon:"https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/a761a66b-74c6-4f5b-b52f-a0ba84ee9bc0.svg",
      name:"MD Editor",
      title: "Untitled.md",
      value: '',
      plugins,
      lang: lang_cn,
      curFileHandle:null,
    }
  },
  watch:{
    curFileHandle(oldHandle,newHandle){
      if(!newHandle) return
      this.title = newHandle.name
      this.value = ''
    }
  },
  methods: {
    onChange(v) {
      this.value = v
    },
    // 新建文件
    async onNewFile(){
      const options = {
        suggestedName: 'Untitled',
        startIn: 'documents',
        types: [
          {
            accept: {
              'text/plain': ['.md'],
            },
          },
        ],
      };
      this.curFileHandle = await window.showSaveFilePicker(options)
      this.curFileHandle && (this.title = this.curFileHandle.name)
    },
    // 打开文件
    async onOpenFile(){
      let [fileHandle] = await window.showOpenFilePicker({
        startIn: 'documents',
      })
      this.curFileHandle = fileHandle
      const fileBlob = await fileHandle.getFile()
      this.value = await fileBlob.text()
      this.title = this.curFileHandle.name
    },
    // 文件保存
    async onSave(){
      await this.writeData(this.curFileHandle,this.value)
    },
    // 文件另存为
    async onSaveAs(){
      const options = {
        suggestedName: 'Untitled',
        startIn: 'desktop',
        types: [
          {
            accept: {
              'text/plain': ['.md'],
            },
          },
        ],
      };
      let handle = await window.showSaveFilePicker(options);
      this.writeData(handle,this.value)
    },
    async writeData(fileHandle, contents) {
      // createWritable()创建一个可写流对象WritableStream
      const writable = await fileHandle.createWritable();
      // 通过管道将数据传输到文件
      await writable.write(contents);
      // 管道使用完毕后需要关闭
      await writable.close();
    }
  },
})

const mdEditorInstance = new MdEditor({store}).$mount()

export default mdEditorInstance