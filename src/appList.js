export default [
  {
    id: "app-document",
    label: "我的文档",
    icon: "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/f5f8f35f-916d-4813-9271-c765994a9a97.png",
    module: ()=>import('@/components/App/Wallpaper/index.js')
  },
  {
    id: "app-wallpaper",
    label: "壁纸",
    icon: "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/ad5de446-06c4-4d3e-9502-a0fa240597c7.png",
    module: ()=>import('@/components/App/Wallpaper/index.js')
  },
  {
    id: "app-mdEditor",
    label: "MD Editor",
    icon: "https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/a761a66b-74c6-4f5b-b52f-a0ba84ee9bc0.svg",
    module: ()=>import('@/components/App/MdEditor/index.js')
  }
]