export default {
  namespaced: true,
  state:()=>({
    backgroundImg:'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/e3d6cdcc-3cbf-4f5d-97c0-459afa1a2c68.jpg',
    backgroundImgList: [
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/e3d6cdcc-3cbf-4f5d-97c0-459afa1a2c68.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/6d195f7e-6d3b-4799-8225-3f1cbc2d4e8c.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/cb0b41dd-dca7-4751-8e91-842b07ec5ab1.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/36ca993b-f518-49a3-93be-725bbc17e841.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/dc67abec-63dc-4f3c-a4f5-40485966b5ed.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/6911ffc3-7050-4a5e-af1d-bad017bd4e3d.jpg',
      'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-391dfedd-5633-4406-9924-79db64ba5599/db9b9e57-cb7a-4039-a409-520ef8e6f45b.jpg'
    ],
  }),
  mutations: {
    CHANGE_BACKGROUND_IMG: (state,newVal)=>{
      state.backgroundImg = newVal
    }
  },
  getters: {

  }
}