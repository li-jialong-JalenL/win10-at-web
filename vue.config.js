'use strict'
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

const port = 9527

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  // hash 模式下可使用
  // publicPath: process.env.NODE_ENV === 'development' ? '/' : './',
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src'),
        'vue$': resolve('node_modules/vue/dist/vue.esm.js'),
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
                    @import '@/style/var.scss';
                    @import '@/style/iconfont.scss';
                    @import '@/style/index.scss';
                `,
      },
    },
  },
}
